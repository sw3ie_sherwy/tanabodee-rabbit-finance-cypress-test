describe('Insurance Matching', () => {
    it('User Complete Insurance Questioning - Success', () => {
        cy.visit('/en/product/health-insurance/questions')

        cy.get('input#product_category-ipdOpd+label').click()

        cy.get('input#product_ipdopd_subcategory-salaryMan+label').click()

        cy.get('input[name="customer_phone"]').type('0999999999')

        cy.get('#customer_phone button[type="button"]').click()

        cy.get('input[name="customer_first_name').type('Tanabodee')
        cy.get('input[name="customer_last_name').type('Leelaprayul')
        cy.get('#customer_first_name button[type="button"]').click()

        cy.get('input#email').type('tanabodee.l@outlook.com')
        cy.get('#customer_email button[type="button"]').click()

        cy.get('input#customer_gender-M+label').click()

        cy.get('input[name="customer_dob"]').type('04/07/1994')
        cy.get('#customer_dob button[type="button"]').click()

        cy.get('input[id="tc-1"]+label').click()

        cy.get('#btn-marketing-consent').click()
        
        //I can't find any way to handle http authen pop up
        //cy.url().should('include', '/product/health-insurance/quotes')
    })
  })